#include <SPI.h>
#include <SD.h>
#include "Wire.h"
#include <Time.h>

// Begin DS1307 RTC
#define DS1307_I2C_ADDRESS 0x68
#define TIME_MSG_LEN 11
#define TIME_HEADER 'T'
// End DS1307 RTC

// Begin SD Card
File myFile;
// End SD Card

// Temp Sensor
#define SENSOR_PIN 0
// End Sensor

char floatStr[9];
char dataStr[24];

void setup() 
{

  Wire.begin();  
  Serial.begin(9600);
  
  Serial.print(F("Initializing SD card..."));

  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(10, OUTPUT);
   
  if (!SD.begin(10)) 
  {
    Serial.print("Init failed!");
    return;
  }
  
  Serial.println(F("initialization done."));
  
   floatStr[0] = '\0';   
  
  analogReference(INTERNAL);
}

void loop() 
{ 
  byte mysecond, myminute, myhour, dayOfWeek, dayOfMonth, mymonth, myyear;
  float temperature, temperature_sum = 0.0F;

  // For setting the clock date and time.  Should only
  // have to do this once, or when adjusting for daylight
  // savings time
  if(Serial.available())
  {
    time_t t = processSyncMessage();
    if(t > 0)
    {
      setTime(t);
      //adjustTime(-21600);  // Subtract 6 hours to go from GMT to US Central
      adjustTime(-18000);  // Subtract 5 hours to go from GMT to US Central DST
      
      mysecond = (byte) second();
      myminute = (byte) minute();
      myhour = (byte) hour();
      dayOfWeek = (byte) weekday();
      dayOfMonth = (byte) day();
      mymonth = (byte) month();
      myyear = (byte) year();
      //Serial.print("The year is ");
      //Serial.println(myyear);
      
      myyear -= 2000;
      
      setDateDs1307(mysecond, myminute, myhour, dayOfWeek, dayOfMonth, mymonth, myyear);      
    }
  }
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.   
  myFile = SD.open("test.txt", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile) 
  {
    Serial.print(F("File opened for writing."));
   } 
   else 
   {
    // if the file didn't open, print an error:
    Serial.println(F("error opening test.txt"));
    return;
  }
  

  getDateDs1307(&mysecond, &myminute, &myhour, &dayOfWeek, &dayOfMonth, &mymonth, &myyear);

  for(int i=0; i< 60; i++)
  {
    int sensor = analogRead(SENSOR_PIN);  

    temperature = ((sensor * 0.1075269) * 1.8) + 32;  // 1100 / 1023 / 10 = 0.1075269
    sprintf(dataStr, "%s F", ftoa(floatStr, temperature, 1));
    temperature_sum += temperature;

    delay(10000);
  }
  
  temperature = temperature_sum / 60;
  temperature_sum = 0.0F;
  
  int decimalTime = (myhour * 100) + myminute;
  
  Serial.println(decimalTime);
    
  // print to file the date/time
  dataStr[0] = '\0';
  
  sprintf(dataStr, "%02d/%02d/%d %02d:%02d:%02d,", mymonth, dayOfMonth, myyear, (int) myhour, (int) myminute, (int) mysecond);

  myFile.print(dataStr);
  Serial.print(dataStr);

  // print to file the temperature
  dataStr[0] = '\0';
  sprintf(dataStr, "%s,F", ftoa(floatStr, temperature, 1));
  
  myFile.println(dataStr);
  Serial.println(dataStr);   

  // close the file
  myFile.close(); 
}



// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}

// writes the date/time to the DS1307 RTC
void setDateDs1307(byte mysecond,        // 0-59
                   byte myminute,        // 0-59
                   byte myhour,          // 1-23
                   byte dayOfWeek,     // 1-7
                   byte dayOfMonth,    // 1-28/29/30/31
                   byte mymonth,         // 1-12
                   byte myyear)          // 0-99
{
   Wire.beginTransmission(DS1307_I2C_ADDRESS);
   Wire.write(0);
   Wire.write(decToBcd(mysecond));    // 0 to bit 7 starts the clock
   Wire.write(decToBcd(myminute));
   Wire.write(decToBcd(myhour));      // If you want 12 hour am/pm you need to set
                                   // bit 6 (also need to change readDateDs1307)
   Wire.write(decToBcd(dayOfWeek));
   Wire.write(decToBcd(dayOfMonth));
   Wire.write(decToBcd(mymonth));
   Wire.write(decToBcd(myyear));
   
   byte ctrlRegByte = 0x13; // 00010000 to produce a 1 Hz square wave 
                            // from the SQW pin.  00010011 for 32.768 kHz.
                            // 00010001 for 4.096 kHz.  The square wave output
                            // can be viewed on an oscilloscope.
   Wire.write(ctrlRegByte);
   Wire.endTransmission();
}

// Gets the date and time from the ds1307
void getDateDs1307(byte *mysecond,
          byte *myminute,
          byte *myhour,
          byte *dayOfWeek,
          byte *dayOfMonth,
          byte *mymonth,
          byte *myyear)
{
  // Reset the register pointer
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.endTransmission();
  
  Wire.requestFrom(DS1307_I2C_ADDRESS, 7);

  // A few of these need masks because certain bits are control bits
  *mysecond     = bcdToDec(Wire.read() & 0x7f);
  *myminute     = bcdToDec(Wire.read());
  *myhour       = bcdToDec(Wire.read() & 0x3f);  // Need to change this if 12 hour am/pm
  *dayOfWeek  = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *mymonth      = bcdToDec(Wire.read());
  *myyear       = bcdToDec(Wire.read());
}


// sets the ds1307 clock date/time.  Set in serial monitor by sending T<unix epooch time>
// Example: T1458122840
time_t processSyncMessage()
{
  // return the time if a valid sync message is received on the serial port.
  // time message consists of a header and ten ascii digits
  while(Serial.available() >= TIME_MSG_LEN)
  {
    char c = Serial.read();
    Serial.print(c);
    if(c == TIME_HEADER)
    {
      time_t pctime = 0;
      for(int i=0; i<TIME_MSG_LEN -1; i++)
      {
        c = Serial.read();
        if(c>='0' && c <='9')
        {
          pctime = (10*pctime) + (c-'0'); // convert digits to a number
        }
      }
      return pctime;
    }    
  }
  return 0;  
}

// converts a float to a null terminated string
char *ftoa(char *a, double f, int precision)
{
  long p[] = {0,10,100,1000,10000,100000,1000000,10000000,100000000};
  
  char *ret = a;
  long heiltal = (long)f;
  itoa(heiltal, a, 10);
  while (*a != '\0') a++;
  *a++ = '.';
  long desimal = abs((long)((f - heiltal) * p[precision]));
  itoa(desimal, a, 10);
  return ret;
}
